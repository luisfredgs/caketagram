<?php
/**
 * InstagramUserFeed Component
 *
 * @author Luis Fred G S <luis.fred.gs@gmail.com>
 * @category Component
 */
namespace CakeTagram\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

use Cake\Network\Http\Client;
use Cake\Cache\Cache;
use Cake\Network\Exception\InternalErrorException;
use Cake\Core\Configure;

/**
 * UserFeed component
 */
class InstagramUserFeedComponent extends Component
{

    /**
     * Access Token
     *
     * @var string
     **/
    public $access_token;


    /**
     * Enable cache feature
     *
     * @var boolean
     **/
    public $use_cache = false;


    /**
     * Retrieve user ID based on username
     */
    protected function getInstaID($username)
    {
        $username = strtolower($username);
        $url = "https://api.instagram.com/v1/users/search";

        $http = new Client();

        $params = [
            'q' => $username,
            'access_token' => $this->access_token
        ];

        $response = $http->get($url, $params, [
            'timeout' => 20
        ]);

        if ($response->isOk()) {
            $json = json_decode($response->body);

            foreach ($json->data as $user) {
                if ($user->username == $username) {
                    return $user->id;
                }
            }
        } else {
            return null;
        }
    }


    /**
     *
     * Retrieve User feed data
     * */
    public function getUserFeed($username)
    {
        if (empty($username)) {
            throw new InternalErrorException("Please. You must provide a username", 500);
        }

        if ($this->use_cache == true && Cache::read('user_feed', 'instagram') !== false) {

            if (Configure::read("CakeTagram.debug") == 1) {
                debug("From Cache");
            }

            $feed = Cache::read('user_feed', 'instagram');

        } else {
            $userID = $this->getInstaID($username);
            $url = "https://api.instagram.com/v1/users/{$userID}/media/recent";

            $http = new Client();

            $params = [
                'access_token' => $this->access_token,
                'count' => 14
            ];

            $response = $http->get($url, $params, [
                'timeout' => 20
            ]);

            if ($response->isOk()) {
                $feed = json_decode($response->body);

                if ($this->use_cache == true) {

                    Cache::write('user_feed', $feed, 'instagram');
                }
            } else {
                $feed = null;
            }
        }

        return $feed;
    }

    /**
     * Retrieve data feed based on hashtag provided
     * @return array
     */
    public function getFeedByHashTag($hashtag)
    {
        try {

            if (empty($this->access_token)) {
                throw new InternalErrorException("Please, provide a access token", 500);
            }

            if (empty($hashtag)) {
                throw new InternalErrorException("Please, provide a hashtag", 500);
            }

            date_default_timezone_set('UTC');

            if ($this->use_cache == true && Cache::read('user_feed_hashtag', 'instagram') !== false) {

                if (Configure::read("CakeTagram.debug") == 1) {
                    debug("From Cache");
                }
                $feed = Cache::read('user_feed_hashtag', 'instagram');

            } else {

                $http = new Client();

                $params = [
                    'access_token' => $this->access_token
                ];

                $response = $http->get('https://api.instagram.com/v1/tags/' . $hashtag . '/media/recent', $params);

                if ($response->isOk()) {
                    $feed = json_decode($response->body);

                    if ($this->use_cache == true) {

                        Cache::write('user_feed_hashtag', $feed, 'instagram');
                    }

                } else {
                    $feed = null;
                }

            }

            return $feed;

        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    /**
     * Clear cache data
     *
     * @return void
     **/
    public function clearCache()
    {
        return Cache::delete('user_feed', 'instagram');
        return Cache::delete('user_feed_hashtag', 'instagram');
    }


}
