<?php
/**
 * CakeTagram bootstrap file
 *
 * @author Luis Fred G S <luis.fred.gs@gmail.com>
 * @category Bootstrap
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;

Cache::config('instagram', [
    'className' => 'File',
    'duration' => 10,
    'path' => CACHE . "instagram",
    'prefix' => 'instagram_'
]);

Configure::write("CakeTagram.debug", 1);
