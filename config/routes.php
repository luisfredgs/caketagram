<?php
use Cake\Routing\Router;

Router::plugin('CakeTagram', function ($routes) {
    $routes->fallbacks('InflectedRoute');
});
